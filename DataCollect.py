import psycopg2,os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from urllib.request import urlopen
from zipfile import ZipFile
import pandas as pd
from datetime import datetime ,timezone

insert_str = """
    "DataCollectiefID","Name1","Name2","Name3","Name4","Name5",
    "Street","HouseNr","HouseNrExt","PostalCode","City","PhoneNumber","KvkNr","KvkResidenceNr","Website","EmailAddress","BranchCode","BranchDescription",
    "NrOfEmployeesID","NrOfEmployeesDescription","CompanyTypeID","CompanyTypeDescription","EconomischActief","WUSStatusID","ContactID","Gender","ContactFullName",
    "ContactFirstName","BusinessFunctionCode","ContactPhoneNumber","ContactMobileNumber","ContactEmailAddress","MainEstablishmentID",
    "Legal","FoundingYear","ProvinceCode","BranchID2","BranchID3","StartDate","CreditStatus","OrganChangeDate","OrganDeleted","OrganDateDeleted",
    "ContactDeleted","ContactDateDeleted","ContactChangeDate","ID" """
# "DateCreated","DateModified",
def create_database_if_not_exist(cur,db_name):
    cur.execute("SELECT datname FROM pg_database;")
    list_database = cur.fetchall()
    if db_name in list_database:
        print("database not exist creating now -----> ")
        sqlCreateDatabase = "create database " + db_name + ";"
        cur.execute(sqlCreateDatabase);

    else:
        print("database already exist")

    conn = psycopg2.connect(
        host="localhost",
        database=db_name,
        user="postgres",
        password="postgres")

    return conn



# connection = psycopg2.connect("user='rohit' host='localhost' password='rohit' port='5432'")
# if connection is not None:
#     connection.autocommit = True
#
# cursor = connection.cursor();
# name_Database = "xyz";
#
# connection = create_database_if_not_exist(cursor,name_Database)
# cursor = connection.cursor();
def all_tables(cursor):
    cursor.execute("""SELECT table_name FROM information_schema.tables
           WHERE table_schema = 'public'""")
    for table in cursor.fetchall():
        print(table)

def download_and_extract_file():
    path = os.path.realpath(__file__)
    path = path.split('/DataCollect.py')[0]
    zipurl = 'http://download.datacollectief.nl/mttm/mttm_Mutaties_DMCT.zip'
    # Download the file from the URL
    zipresp = urlopen(zipurl)
    # Create a new file on the hard drive
    tempzip = open("/tmp/tempfile.zip", "wb")
    # Write the contents of the downloaded file into the new file
    tempzip.write(zipresp.read())
    # Close the newly-created file
    tempzip.close()
    # Re-open the newly-created file with ZipFile()
    zf = ZipFile("/tmp/tempfile.zip")
    # Extract its contents into <extraction_path>
    # note that extractall will automatically create the path
    zf.extractall(path=path)
    # close the ZipFile instance
    zf.close()

def create_table(conn, table):
    cur = conn.cursor()
    cur.execute("select * from information_schema.tables where table_name='%s'"%(table))
    if cur.rowcount != 0:
        print("tbale already exist using existing one ")
    else:
        cur.execute(open("Datacollectief table.sql", "r").read())

    all_tables(cur)
    conn.commit()
    cur.close()

def convert_txt_to_csv(file):
    data = pd.read_csv(file, header=None)
    # data.columns = ["DataCollectiefID","Name1","Name2","Name3","Name4","Name5","Street","HouseNr","HouseNrExt",PostalCode,City,PhoneNumber,KvkNr,KvkResidenceNr,]
    pass

def update_sample_a_row(conn,cur):
    d = datetime.today()
    qry = """update "Datacollectief" set 
       "Name1" = '%s',
       "OrganDateDeleted" = (TIMESTAMP '%s')
       where "ID" = '%s'
       """%("xxxx- Meijer Beheer BV",d,1)
    cur.execute(qry)
    conn.commit()

def update_a_row(cur,line,company_id,contact_id):
    id = line[0]
    qry = """update "Datacollectief" set 
    "DataCollectiefID" = '%s',
    "Name1" = '%s',
    "Name2" = '%s',
    "Name3" = '%s',
    "Name4" = '%s',
    "Name5" = '%s',
    "Street" = '%s',
    "HouseNr" = '%s',
    "HouseNrExt" = '%s',
    "PostalCode" = '%s',
    "City" = '%s',
    "PhoneNumber" = '%s',
    "KvkNr" = '%s',
    "KvkResidenceNr" = '%s' ,
    "Website" = '%s' ,
    "EmailAddress" = '%s' ,
    "BranchCode" = '%s' ,
    "BranchDescription" = '%s',
    "NrOfEmployeesID" = '%s' ,
    "NrOfEmployeesDescription" = '%s',
    "CompanyTypeID" = '%s' ,
    "CompanyTypeDescription" = '%s',
    "EconomischActief" = '%s',
    "WUSStatusID" = '%s' ,
    "ContactID" = '%s' ,
    "Gender" = '%s',
    "ContactFullName" = '%s',
    "ContactFirstName" = '%s',
    "BusinessFunctionCode" = '%s',
    "ContactPhoneNumber" = '%s',
    "ContactMobileNumber" = '%s' ,
    "ContactEmailAddress" = '%s',
    "MainEstablishmentID" = '%s',
    "Legal" = '%s',
    "FoundingYear" = '%s',
    "ProvinceCode" = '%s',
    "BranchID2" = '%s',
    "BranchID3" = '%s',
    "StartDate" = (TIMESTAMP '%s'),
    "CreditStatus" = '%s',
    "OrganChangeDate" = (TIMESTAMP '%s'),
    "OrganDeleted" = '%s',
    "OrganDateDeleted" = (TIMESTAMP '%s'),
    "ContactDeleted" = '%s',
    "ContactDateDeleted" = (TIMESTAMP '%s'),
    "ContactChangeDate" = (TIMESTAMP '%s') 
    
    where "DataCollectiefID" = '%s' and "ContactID" = '%s'   
    """

    qry = qry % tuple(line[:-1] + [company_id,contact_id])
    # KvkNr2 = %s,
    # ID = '%s'  ,
    # DateCreated = % s,
    # DateModified = % s,
    cur.execute(qry)
    conn.commit()

def create_or_update_table(conn,data,all_ids):
    cur = conn.cursor()


    all_dict = {}
    for i in all_ids:
        if i[0] not in all_ids:
            all_dict[i[0]] = [i[1]]
        else:
            all_dict[i[0]].append(i[1])

    records_to_create = []
    i = 0
    updatec = 1
    for line in data:
        company_id , contact_id = line[0], line[24]
        if company_id not in all_dict:
            records_to_create.append(line)
        else:
            if contact_id not in all_dict[company_id]:
                records_to_create.append(line)
            else:
                try:
                    update_a_row(cur,line,company_id,contact_id)
                    # update_sample_a_row(conn,cur)
                    # break
                    print("update row %d"%(updatec))
                    updatec += 1
                except Exception as e:
                    print("Could not update row = %d"%(updatec))
    if records_to_create:
        # s = ",".join("%s" for i in range(38))
        # s += ",(TIMESTAMP '%s'),%s,(TIMESTAMP '%s'),%s,(TIMESTAMP '%s'),%s,(TIMESTAMP '%s'),(TIMESTAMP '%s')"
        # s =  s + ",%s)"
        s = ",".join(" '%s' " for i in range(38))
        s += ",(TIMESTAMP '%s'),'%s',(TIMESTAMP '%s'),'%s',(TIMESTAMP '%s'),'%s',(TIMESTAMP '%s'),(TIMESTAMP '%s')"
        s = s + ",'%s'"
        # s = "%d,"+s
        # s = '%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,' \
        #     '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,' \
        #     '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,' \
        #     '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,' \
        #     '%s,%s'
        # args_str = ','.join(cur.mogrify(s, x) for x in records_to_create)
        args_str = ""
        # for i , x in enumerate(records_to_create):
        #     try:
        #         args_str += cur.mogrify(s, x)
        #         if i != len(records_to_create - 1):
        #             args_str += ","
        #     except Exception as e:
        #         print("this %s has problem"%(x))
        #         pass
        # cur.executemany("""INSERT INTO "Datacollectief" (%s) VALUES """%(insert_str) + s, records_to_create)
        success = 0
        fail = 0
        for x,i in enumerate(records_to_create):
            # ss = """INSERT INTO "Datacollectief" (%s) VALUES %s""" % (insert_str, s, i)
            # cur.execute(ss)
            # print("%d done" % (x))
            # ss = """INSERT INTO "Datacollectief" (%s) VALUES (%s)""" % (insert_str, s)
            # ss = ss % tuple(i)
            # cur.execute(ss)
            # print("%s done" % (x))
            ss = 0
            try:
                ss = """INSERT INTO "Datacollectief" (%s) VALUES (%s)"""%(insert_str,s)
                ss = ss % tuple(i)
                cur.execute(ss)
                print("%s done"%(x))
                success += 1
            except Exception as e:
                fail += 1
                print("%s --> Exception : %s"%(x,e))
        # i += 1
        # if i == 10:
        #     break
    print("Fail - %d , success  - %d"%(fail,success))
    conn.commit()
    cur.close()




def update_data_in_Datacollecdtief_table(conn):
    # DataCollectiefID,Name1,Name2,Name3,Name4,Name5,Street,HouseNr,HouseNrExt,PostalCode,City,PhoneNumber,KvkNr,KvkResidenceNr,
    # convert_txt_to_csv("MTTM_mutaties.txt")
    data = []
    cur = conn.cursor()

    cur.execute("""select "DataCollectiefID","ContactID" from "Datacollectief";""")
    all_ids = cur.fetchall()
    ln = len(all_ids)
    with open("MTTM_mutaties.txt","r",encoding="utf8", errors='ignore') as f:
        for line in f:
            # line = f.readline()
            # if not line:break
            # line1 = [i.replace('"', '') for i  in line.split("|")]
            line1  =[]
            for i in line.split("|"):
                if i:
                    if i == 0:
                        line1.append(int(i.replace('"', '').replace("'",'')))
                    else:
                        line1.append(i.replace('"', '').replace("'",''))
                else:
                    line1.append(i)

            # if len(line1) < 50:
            #     df = 49 - len(line1)
            #     for j in range(df):
            #         line1.append('')
            ln += 1
            line1.append(ln)
            line1[0] = int(line1[0])
            try:
                line1[38] = datetime.strptime(line1[38].replace("\n",''), '%b %d %Y %I:%M%p')
                line1[40] = datetime.strptime(line1[40].replace("\n",''), '%b %d %Y %I:%M%p')
                line1[42] = datetime.strptime(line1[42].replace("\n",''), '%b %d %Y %I:%M%p')
                line1[44] = datetime.strptime(line1[44].replace("\n",''), '%b %d %Y %I:%M%p')
                line1[45] = datetime.strptime(line1[45].replace("\n",''), '%b %d %Y %I:%M%p')
            except Exception as e:
                pass
            data.append(line1)
    cur.close()
    create_or_update_table(conn,data,all_ids)

def insert_one_row(conn):
    cur = conn.cursor()
    dt = datetime.now().strftime("%b %d %Y %I:%M%p")
    # dt = datetime.now()
    # ss = """INSERT INTO "Datacollectief" ("OrganChangeDate","ID") VALUES (to_timestamp('%s', 'dd-mm-yyyy hh24:mi:ss'),%s)""" % (dt,1)
    ss = """INSERT INTO "Datacollectief" ("OrganChangeDate","ID") VALUES ((TIMESTAMP '%s'),%s)""" % (dt,3)
    # ss = """INSERT INTO "Datacollectief" ("OrganChangeDate","ID") VALUES ((TIMESTAMP '%s'),%s)"""
    cur.execute(ss)
    # cur.execute(ss,[dt,3])
    conn.commit()
    conn.close()
    pass

conn = psycopg2.connect(database="stephen", user="rohit", password="stephen")
# download_and_extract_file()
create_table(conn, "Datacollectief")
# insert_one_row(conn)
update_data_in_Datacollecdtief_table(conn)
# update_sample_a_row(conn,conn.cursor())