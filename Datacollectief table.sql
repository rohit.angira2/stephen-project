-- Table: public.Datacollectief

-- DROP TABLE public."Datacollectief";

CREATE TABLE public."Datacollectief"
(
    "DataCollectiefID" bigint,
    "Name1" character varying(255) COLLATE pg_catalog."default",
    "Name2" character varying(255) COLLATE pg_catalog."default",
    "Name3" character varying(255) COLLATE pg_catalog."default",
    "Name4" character varying(255) COLLATE pg_catalog."default",
    "Name5" character varying(255) COLLATE pg_catalog."default",
    "Street" character varying(255) COLLATE pg_catalog."default",
    "HouseNr" character varying(255) COLLATE pg_catalog."default",
    "HouseNrExt" character varying(255) COLLATE pg_catalog."default",
    "PostalCode" character varying(255) COLLATE pg_catalog."default",
    "City" character varying(255) COLLATE pg_catalog."default",
    "PhoneNumber" character varying(255) COLLATE pg_catalog."default",
    "KvkNr" character varying(255) COLLATE pg_catalog."default",
    "KvkResidenceNr" character varying(255) COLLATE pg_catalog."default",
    "Website" character varying(255) COLLATE pg_catalog."default",
    "EmailAddress" character varying(255) COLLATE pg_catalog."default",
    "BranchCode" character varying(255) COLLATE pg_catalog."default",
    "BranchDescription" character varying(255) COLLATE pg_catalog."default",
    "NrOfEmployeesID" character varying(255) COLLATE pg_catalog."default",
    "NrOfEmployeesDescription" character varying(255) COLLATE pg_catalog."default",
    "CompanyTypeID" character varying(255) COLLATE pg_catalog."default",
    "CompanyTypeDescription" character varying(255) COLLATE pg_catalog."default",
    "EconomischActief" character varying(255) COLLATE pg_catalog."default",
    "WUSStatusID" character varying(255) COLLATE pg_catalog."default",
    "ContactID" character varying(255) COLLATE pg_catalog."default",
    "Gender" character varying(50) COLLATE pg_catalog."default",
    "ContactFullName" character varying(255) COLLATE pg_catalog."default",
    "ContactFirstName" character varying(255) COLLATE pg_catalog."default",
    "BusinessFunctionCode" character varying(255) COLLATE pg_catalog."default",
    "ContactPhoneNumber" character varying(255) COLLATE pg_catalog."default",
    "ContactMobileNumber" character varying(255) COLLATE pg_catalog."default",
    "ContactEmailAddress" character varying(255) COLLATE pg_catalog."default",
    "MainEstablishmentID" character varying(255) COLLATE pg_catalog."default",
    "Legal" character varying(255) COLLATE pg_catalog."default",
    "FoundingYear" character varying(20) COLLATE pg_catalog."default",
    "ProvinceCode" character varying(255) COLLATE pg_catalog."default",
    "BranchID2" character varying(255) COLLATE pg_catalog."default",
    "BranchID3" character varying(255) COLLATE pg_catalog."default",
    "StartDate" timestamp(6) with time zone,
    "CreditStatus" character varying(255) COLLATE pg_catalog."default",
    "OrganChangeDate" timestamp(6) with time zone,
    "OrganDeleted" character varying(255) COLLATE pg_catalog."default",
    "OrganDateDeleted" timestamp(6) with time zone,
    "ContactDeleted" character varying(255) COLLATE pg_catalog."default",
    "ContactDateDeleted" timestamp(6) with time zone,
    "ContactChangeDate" timestamp(6) with time zone,
    "KvkNr2" character varying(255) COLLATE pg_catalog."default",
    "DateCreated" timestamp(6) with time zone,
    "DateModified" timestamp(6) with time zone,
--    "ID" bigint NOT NULL DEFAULT nextval('"Datacollectief_ID_seq"'::regclass),
    "ID" bigint NOT NULL ,
    CONSTRAINT "Datacollectief_pkey" PRIMARY KEY ("ID")
)

TABLESPACE pg_default;

ALTER TABLE public."Datacollectief"
    OWNER to postgres;